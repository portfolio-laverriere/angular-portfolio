export class Video {

  id: number;
  title: string;
  videoUrl: string;

  constructor(id: number, title: string, videoUrl: string) {
    this.id = id;
    this.title = title;
    this.videoUrl = videoUrl;
  }
}
