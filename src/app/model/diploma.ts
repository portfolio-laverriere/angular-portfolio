import {Image} from './image';

export class Diploma {

  id: number;
  title: string;
  obtaining: string;
  school: string;
  city: string;
  paragraph: string;
  profilId: number;
  imageDTOList: Image[];

  constructor(id: number, title: string, obtaining: string, school: string, city: string, paragraph: string, profilId: number, imageDTOList: Image[]) {
    this.id = id;
    this.title = title;
    this.obtaining = obtaining;
    this.school = school;
    this.city = city;
    this.paragraph = paragraph;
    this.profilId = profilId;
    this.imageDTOList = imageDTOList;
  }
}
