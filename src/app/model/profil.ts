import {Competence} from './competence';
import {Conference} from './conference';
import {Diploma} from './diploma';
import {Experience} from './experience';
import {PointOfInterest} from './point-of-interest';
import {SocialNetwork} from './social-network';

export class Profil {

  id: number;
  lastName: string;
  firstName: string;
  adress: string;
  dateOfBirth: string;
  phoneNumber: string;
  email: string;
  competenceDTOList: Competence[];
  conferenceDTOList: Conference[];
  diplomaDTOList: Diploma[];
  experienceDTOList: Experience[];
  pointOfInterestDTOList: PointOfInterest[];
  socialNetworkDTOList: SocialNetwork[];

  constructor(id: number, lastName: string, firstName: string, adress: string, dateOfBirth: string, phoneNumber: string,
              email: string, competenceDTOList: Competence[], conferenceDTOList: Conference[], diplomaDTOList: Diploma[],
              experienceDTOList: Experience[], pointOfInterestDTOList: PointOfInterest[], socialNetworkDTOList: SocialNetwork[]) {
    this.id = id;
    this.lastName = lastName;
    this.firstName = firstName;
    this.adress = adress;
    this.dateOfBirth = dateOfBirth;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.competenceDTOList = competenceDTOList;
    this.conferenceDTOList = conferenceDTOList;
    this.diplomaDTOList = diplomaDTOList;
    this.experienceDTOList = experienceDTOList;
    this.pointOfInterestDTOList = pointOfInterestDTOList;
    this.socialNetworkDTOList = socialNetworkDTOList;
  }
}
