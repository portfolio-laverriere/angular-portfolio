import {Image} from './image';

export class Paragraph {

  id: number;
  subtitle: string;
  paragraph: string;
  position: number;
  articleId: number;
  imageDTOList: Image[];

  constructor(id: number, subtitle: string, paragraph: string, position: number, articleId: number, imageDTOList: Image[]) {
    this.id = id;
    this.subtitle = subtitle;
    this.paragraph = paragraph;
    this.position = position;
    this.articleId = articleId;
    this.imageDTOList = imageDTOList;
  }
}
