import {Tag} from './tag';

export class PointOfInterest {

  id: number;
  name: string;
  paragraph: string;
  profilId: number;
  tagDTOList: Tag[];

  constructor(id: number, name: string, paragraph: string, profilId: number, tagDTOList: Tag[]) {
    this.id = id;
    this.name = name;
    this.paragraph = paragraph;
    this.profilId = profilId;
    this.tagDTOList = tagDTOList;
  }
}
