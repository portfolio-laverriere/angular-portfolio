import {Resource} from './resource';
import {Tag} from './tag';
import {Paragraph} from './paragraph';

export class Article {

  id: number;
  title: string;
  subtitle: string;
  paragraph: string;
  position: number;
  projectId: number;
  proofOfConceptId: number;
  paragraphDTOList: Paragraph[];
  tagDTOList: Tag[];
  resourceDTOList: Resource[];

  constructor(id: number, title: string, subtitle: string, paragraph: string, position: number, projectId: number,
              proofOfConceptId: number, paragraphDTOList: Paragraph[], tagDTOList: Tag[], resourceDTOList: Resource[]) {
    this.id = id;
    this.title = title;
    this.subtitle = subtitle;
    this.paragraph = paragraph;
    this.position = position;
    this.projectId = projectId;
    this.proofOfConceptId = proofOfConceptId;
    this.paragraphDTOList = paragraphDTOList;
    this.tagDTOList = tagDTOList;
    this.resourceDTOList = resourceDTOList;
  }
}
