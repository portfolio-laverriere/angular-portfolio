export class Competence {

  id: number;
  name: string;
  level: number;
  numberOfPracticeMonths: number;
  profilId: number;

  constructor(id: number, name: string, level: number, numberOfPracticeMonths: number, profilId: number) {
    this.id = id;
    this.name = name;
    this.level = level;
    this.numberOfPracticeMonths = numberOfPracticeMonths;
    this.profilId = profilId;
  }
}
