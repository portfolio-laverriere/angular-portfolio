export class Tag {

  id: number;
  name: string;
  tagPath: string;

  constructor(id: number, name: string, tagPath: string) {
    this.id = id;
    this.name = name;
    this.tagPath = tagPath;
  }
}
