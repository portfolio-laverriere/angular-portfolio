import {Image} from './image';

export class Experience {

  id: number;
  title: string;
  company: string;
  commencementDate: string;
  stopDate: string;
  city: string;
  paragraph: string;
  profilId: number;
  imageDTOList: Image[];

  constructor(id: number, title: string, company: string, commencementDate: string, stopDate: string, city: string, paragraph: string, profilId: number, imageDTOList: Image[]) {
    this.id = id;
    this.title = title;
    this.company = company;
    this.commencementDate = commencementDate;
    this.stopDate = stopDate;
    this.city = city;
    this.paragraph = paragraph;
    this.profilId = profilId;
    this.imageDTOList = imageDTOList;
  }
}
