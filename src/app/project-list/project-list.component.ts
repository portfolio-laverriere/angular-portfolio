import { Component, OnInit } from '@angular/core';
import {Project} from '../model/project';
import {ProjectService} from '../service/project.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
  providers: [DatePipe]
})
export class ProjectListComponent implements OnInit {

  projectList: Project[];

  constructor(private projectService: ProjectService, public datePipe: DatePipe) { }

  ngOnInit(): void {
    this.onFetchProjectList();
  }

  onClickPojectDetail(project: Project) {
    this.projectService.setProject(project);
  }

  onFetchProjectList() {
    this.projectService.getAllPojectFromServer().subscribe(
      (httpResponse) => {
        this.projectList = httpResponse;
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }
}
