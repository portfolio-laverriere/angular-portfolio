import {Component, OnInit} from '@angular/core';
import {Project} from '../../model/project';
import {ProjectService} from '../../service/project.service';
import {Article} from '../../model/article';


@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})

export class ProjectDetailComponent implements OnInit {

  project: Project;

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.project = this.projectService.getProject();
    this.onFetchAllArticleByProject(this.project.id);
  }

  onFetchAllArticleByProject(projectId: number): void {
    this.projectService.getAllArticleByProjectFromServer(projectId).subscribe(
      (httpResponse: Article[]) => {
        this.project.articleDTOList = httpResponse;
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }
}


