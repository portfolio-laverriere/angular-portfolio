import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProofOfConceptListComponent } from './proof-of-concept-list.component';

describe('ProofOfConceptListComponent', () => {
  let component: ProofOfConceptListComponent;
  let fixture: ComponentFixture<ProofOfConceptListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProofOfConceptListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProofOfConceptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
