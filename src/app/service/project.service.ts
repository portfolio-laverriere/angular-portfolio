import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Project} from '../model/project';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Article} from '../model/article';

@Injectable()
export class ProjectService {

  private project: Project;

  getProject(): Project {
    return this.project;
  }

  setProject(value: Project) {
    this.project = value;
  }

  constructor(private httpClient: HttpClient) {}

  getAllPojectFromServer(): Observable<Project[]> {
    return this.httpClient
      .get<Project[]>('http://localhost:9102/MICROSERVICE-PROJECT/project/project/get-all-project')
      .pipe(map(httpResponse => httpResponse));
  }

  getAllArticleByProjectFromServer(projectId): Observable<Article[]> {
    return this.httpClient
      .get<Article[]>('http://localhost:9102/MICROSERVICE-ARTICLE/article/project/get-all-article/' + projectId)
      .pipe(map(httpResponse => httpResponse));
  }
}
