import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Article} from '../model/article';
import {ProofOfConcept} from '../model/proof-of-concept';

@Injectable()
export class ProofOfConceptService {

  private proofOfConcept: ProofOfConcept;

  getProofOfConcept(): ProofOfConcept {
    return this.proofOfConcept;
  }

  setProofOfConcept(value: ProofOfConcept) {
    this.proofOfConcept = value;
  }

  constructor(private httpClient: HttpClient) {}

  getAllProofOfConceptFromServer(): Observable<ProofOfConcept[]> {
    return this.httpClient
      .get<ProofOfConcept[]>('http://localhost:9102/MICROSERVICE-PROOF-OF-CONCEPT/poc/poc/get-all-poc')
      .pipe(map(httpResponse => httpResponse));
  }

  getAllArticleByProofOfConceptFromServer(proofOdConceptId): Observable<Article[]> {
    return this.httpClient
      .get<Article[]>('http://localhost:9102/MICROSERVICE-ARTICLE/article/poc/get-all-article/' + proofOdConceptId)
      .pipe(map(httpResponse => httpResponse));

  }
}
