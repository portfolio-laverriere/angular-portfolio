import { Component, OnInit } from '@angular/core';
import {ProjectService} from '../service/project.service';
import {Project} from '../model/project';
import {ProofOfConceptService} from '../service/proofOfConcept.service';
import {ProofOfConcept} from '../model/proof-of-concept';
import {ProfilService} from '../service/profil.service';
import {Profil} from '../model/profil';
import {Competence} from '../model/competence';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {

  projectList: Project[] = [];
  proofOfConceptList: ProofOfConcept[];
  profil: Profil;
  competenceList: Competence[] = [];

  constructor(private projectService: ProjectService, private proofOfConceptService: ProofOfConceptService,
              private profilService: ProfilService) { }

  ngOnInit(): void {
    this.onFetchProjectList();
    this.onFetchProofOfConceptList();
    this.onFetchProfil();
  }

  onClickPojectDetail(project: Project) {
    this.projectService.setProject(project);
  }

  onClickProofOfConceptDetail(proofOfConcept: ProofOfConcept) {
    this.proofOfConceptService.setProofOfConcept(proofOfConcept);
  }

  onFetchProjectList() {

    this.projectService.getAllPojectFromServer().subscribe(
      (httpResponse) => {
        for (let i = 0; i < 3; i++) {
          if (httpResponse[i]) {
            this.projectList.push(httpResponse[i]);
          }
        }
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }

  onFetchProofOfConceptList() {
    this.proofOfConceptService.getAllProofOfConceptFromServer().subscribe(
      (httpResponse) => {
        this.proofOfConceptList = httpResponse;
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }

  onFetchProfil() {
    this.profilService.getProfilFromServer().subscribe(
      (httpResponse) => {
        this.profil = httpResponse;
        for (let i = 0; i < 5; i++) {
          if (httpResponse.competenceDTOList[i]) {
            this.competenceList.push(httpResponse.competenceDTOList[i]);
          }
        }
      },
      (error) => {
        console.log('Error : ' + error);
      }
    );
  }
}
