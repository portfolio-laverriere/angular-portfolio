import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import {ChartsModule, MDBBootstrapModule} from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthConfigModule } from './auth/auth-config.module';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthInitializer } from './auth-initializer';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjectService } from './service/project.service';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-list/project-detail/project-detail.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AccueilComponent } from './accueil/accueil.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProofOfConceptService } from './service/proofOfConcept.service';
import { ProofOfConceptListComponent } from './proof-of-concept-list/proof-of-concept-list.component';
import { ProofOfConceptDetailComponent } from './proof-of-concept-list/proof-of-concept-detail/proof-of-concept-detail.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ProfilComponent } from './profil/profil.component';
import {ProfilService} from './service/profil.service';


@NgModule({
  declarations: [
    AppComponent,
    UnauthorizedComponent,
    AppComponent,
    ProjectListComponent,
    ProjectDetailComponent,
    HeaderComponent,
    FooterComponent,
    AccueilComponent,
    ProofOfConceptListComponent,
    ProofOfConceptDetailComponent,
    ProfilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthConfigModule,
    ChartsModule,
    NgbModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://localhost:9102'],
        sendAccessToken: true
      }
    }),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot()
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    ProjectService,
    ProfilService,
    ProofOfConceptService,
    {
      provide: APP_INITIALIZER,
      useFactory(authInitializer: AuthInitializer) {
        return () => authInitializer.init();
      },
      deps: [AuthInitializer],
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
